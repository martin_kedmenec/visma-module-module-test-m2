# @license Proprietary
# @author Marcus Irgens Pedersen <marcus.pedersen@visma.com>
# @author Kenneth Susort <kenneth.susort@visma.com>
# @author Patryk Makowski <patryk.makowski@visma.com>
# @copyright Visma Digital Commerce 2021

# @pipeline-version 0.2.5

image:
  # Remember to set the correct PHP version
  # Remember to set the correct artifact version used in steps after executed after "build" steps
  name: 'docker.trolldev.infra.polarnordic.com/visma/bitbucket-pipelines-m2-mod:7.4'
  username: $DOCKER_REGISTRY_USER
  password: $DOCKER_REGISTRY_PASSWORD

# Clone full repo instead of shallow copy - required for git commands (e.g. in documentation checks)
# https://community.atlassian.com/t5/Bitbucket-questions/fatal-bad-revision-when-using-git-log-with-BITBUCKET-COMMIT/qaq-p/886297
clone:
  depth: full

pipelines:
  default:
    - parallel:
        - step:
            name: build-php-7.4
            image:
              name: 'docker.trolldev.infra.polarnordic.com/visma/bitbucket-pipelines-m2-mod:7.4'
              username: $DOCKER_REGISTRY_USER
              password: $DOCKER_REGISTRY_PASSWORD
            # More about caches https://support.atlassian.com/bitbucket-cloud/docs/cache-dependencies/
            caches:
              - composer
              - node
            script:
              # Install build dependencies
              - 'apk add -u unzip curl git tar npm'
              - 'curl -LsS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer'
              - 'composer -V'
              # Prepare git repository
              - 'git checkout master'
              - 'git checkout $BITBUCKET_BRANCH'
              # Set up Composer repositories
              - 'composer config -a http-basic.repo.magento.com $MAGENTO_REPO_USER $MAGENTO_REPO_PASSWORD'
              - 'composer config -a http-basic.api.packages.vdc-services.io $VDC_REPO_USER $VDC_REPO_PASSWORD'
              # Require PHP 7.4 version
              - 'composer require --no-update --dev "php":"~7.4.0"'
              # Require Magento "magento/product-enterprise-edition": "2.4.2"
              - 'composer require --no-update --dev "magento/framework":"103.0.*"'
              # Validate composer.json
              - 'composer validate --no-interaction --no-ansi'
              # Install dependencies
              - 'composer install --no-suggest --no-progress --no-interaction --no-ansi'
              # Shows installed packages (for debugging)
              - 'composer show --no-ansi --no-interaction'
              # Package the the code for use in subsequent steps
              - 'mkdir -p .package'
              - 'tar --exclude=.package/build-php-7.4.tar -cf .package/build-php-7.4.tar .'
            artifacts:
              - .package/build-php-7.4.tar
        - step:
            name: build-php-7.3
            image:
              name: 'docker.trolldev.infra.polarnordic.com/visma/bitbucket-pipelines-m2-mod:7.3'
              username: $DOCKER_REGISTRY_USER
              password: $DOCKER_REGISTRY_PASSWORD
            # More about caches https://support.atlassian.com/bitbucket-cloud/docs/cache-dependencies/
            caches:
              - composer
              - node
            script:
              # Install build dependencies
              - 'apk add -u unzip curl git tar npm'
              - 'curl -LsS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --1'
              # Set up Composer repositories
              - 'composer config -a http-basic.repo.magento.com $MAGENTO_REPO_USER $MAGENTO_REPO_PASSWORD'
              - 'composer config -a http-basic.api.packages.vdc-services.io $VDC_REPO_USER $VDC_REPO_PASSWORD'
              # Require PHP 7.3 version
              - 'composer require --no-update --dev "php":"~7.3.0"'
              # Require Magento "magento/product-enterprise-edition": "2.3.4-p2"
              - 'composer require --no-update --dev "magento/framework":"102.0.*"'
              # Remove modules not compatible with the given PHP version
              - 'composer remove --no-update --dev "infection/infection"'
              # Validate composer.json
              - 'composer validate --no-interaction --no-ansi'
              # Install parallel downloader for packages
              - 'composer global require hirak/prestissimo --quiet --no-interaction --no-ansi'
              # Install dependencies
              - 'composer install --no-suggest --no-progress --no-interaction --no-ansi'
              # Shows installed packages (for debugging)
              - 'composer show --no-ansi --no-interaction'
              # Package the the code for use in subsequent steps
              - 'mkdir -p .package'
              - 'tar --exclude-vcs --exclude=.package/build-php-7.3.tar -cf .package/build-php-7.3.tar .'
            artifacts:
              - .package/build-php-7.3.tar
    # Actual testing is performed in these steps.
    # Remove the steps that are not used (eg. remove unit tests if you don't have PHPUnit configured)
    - parallel:
        - step:
            name: 'Documentation'
            script:
              - 'apk add -u tar grep git wget'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'rm -rf .package'
              # Check if CHANGELOG.md adjusted
              - 'test -s CHANGELOG.md'
              - '(git branch --show-current | grep "master") || (git --no-pager diff --name-only origin/master...origin/$BITBUCKET_BRANCH -- | grep CHANGELOG.md)'
              # Check if README.md exists
              - 'test -s README.md'
              # Check if PHP code is documented
              - 'php vendor/bin/php-doc-check --exclude vendor --exclude doc --exclude app --exclude node_modules --exclude .tmp ./'
              # Generate PHP Documentation (available via composer but not recommended because of conflicts)
              - 'wget https://github.com/phpDocumentor/phpDocumentor/releases/download/v3.1.2/phpDocumentor.phar --directory-prefix=.tmp/.tools'
              - 'rm -rf doc/phpdoc/*'
              - 'php .tmp/.tools/phpDocumentor.phar -d src -t doc/phpdoc --cache-folder=./tmp/.phpdoc --defaultpackagename=visma'
              - 'git restore -- composer.json; git --no-pager diff'
              - 'git add --all doc/phpdoc'
              - '(git status --porcelain | grep .) && (git commit -m "[skip ci] Updating PhpDoc" && git push) || echo "No changes to commit or push"'
        - step:
            name: 'PHP Code Style Checks'
            script:
              - 'apk add -u tar'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'rm -rf .package'
              - 'mkdir -p test-results'
              # This line executes phpcs and reports the results in the JUnit format for Bitbucket.
              - 'php vendor/bin/phpcs --report-junit=./test-results/phpcs.xml'
        - step:
            name: 'PHP Mess Detector'
            script:
              - 'apk add -u tar'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'rm -rf .package'
              - 'mkdir -p test-results'
              # This line executes PHP Mess Detector and reports the results in the terminal with ANSI coloring
              - 'vendor/bin/phpmd . ansi phpmd.xml'
        - step:
            name: 'Psalm'
            script:
              - 'apk add -u tar'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'rm -rf .package'
              - 'mkdir psalm-cache'
              - 'mkdir -p test-results'
              # This line executes Psalm
              - 'vendor/bin/psalm -m --no-progress --show-info=true --no-cache --no-file-cache --no-reflection-cache --output-format=xml --report=./test-results/psalm.xml'
        - step:
            name: 'PHP Security Check 7.4'
            caches:
              - tools
            script:
              - 'apk add -u tar curl bash git'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'mkdir -p .tmp/.tools'
              - "[ -f \".tmp/.tools/symfony\" ] || (/usr/bin/env curl -sS https://get.symfony.com/cli/installer --output .tmp/.tools/symfony-installer && /usr/bin/env bash .tmp/.tools/symfony-installer --install-dir=.tmp/.tools && /usr/bin/env rm -f .tmp/.tools/symfony-installer)"
              # Execute Symfony Security Checks
              - '.tmp/.tools/symfony security:check --dir .'
              # Execute Snyk PHP Checks
              - pipe: snyk/snyk-scan:master
                variables:
                  SNYK_TOKEN: $SNYK_TOKEN
                  LANGUAGE: composer
                  TARGET_FILE: "composer.lock"
                  MONITOR: $SNYK_MONITOR
                  SEVERITY_TRESHOLD: $SNYK_SEVERITY_TRESHOLD
                # Execute Snyk NPM Checks
              - pipe: snyk/snyk-scan:master
                variables:
                  SNYK_TOKEN: $SNYK_TOKEN
                  LANGUAGE: npm
                  TARGET_FILE: "package-lock.json"
                  MONITOR: $SNYK_MONITOR
                  SEVERITY_TRESHOLD: $SNYK_SEVERITY_TRESHOLD
        - step:
            name: 'PHP Security Check 7.3'
            caches:
              - tools
            script:
              - 'apk add -u tar curl bash git'
              - 'tar -xf .package/build-php-7.3.tar'
              - 'mkdir -p .tmp/.tools'
              - "[ -f \".tmp/.tools/symfony\" ] || (/usr/bin/env curl -sS https://get.symfony.com/cli/installer --output .tmp/.tools/symfony-installer && /usr/bin/env bash .tmp/.tools/symfony-installer --install-dir=.tmp/.tools && /usr/bin/env rm -f .tmp/.tools/symfony-installer)"
              # Execute Symfony Security Checks
              - '.tmp/.tools/symfony security:check --dir .'
              # Execute Snyk PHP Checks
              - pipe: snyk/snyk-scan:master
                variables:
                  SNYK_TOKEN: $SNYK_TOKEN
                  LANGUAGE: composer
                  TARGET_FILE: "composer.lock"
                  MONITOR: $SNYK_MONITOR
                  SEVERITY_TRESHOLD: $SNYK_SEVERITY_TRESHOLD
                # Execute Snyk NPM Checks
              - pipe: snyk/snyk-scan:master
                variables:
                  SNYK_TOKEN: $SNYK_TOKEN
                  LANGUAGE: npm
                  TARGET_FILE: "package-lock.json"
                  MONITOR: $SNYK_MONITOR
                  SEVERITY_TRESHOLD: $SNYK_SEVERITY_TRESHOLD
        - step:
            name: 'PHP Unit Tests 7.4'
            image:
              name: 'docker.trolldev.infra.polarnordic.com/visma/bitbucket-pipelines-m2-mod:7.4'
              username: $DOCKER_REGISTRY_USER
              password: $DOCKER_REGISTRY_PASSWORD
            script:
              - 'apk add -u tar'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'rm -rf .package'
              - 'mkdir -p test-results'
              # This line executes PHPUnit and reports the results in the JUnit format for Bitbucket.
              - 'php vendor/bin/phpunit --log-junit ./test-results/phpunit-php-7.4.xml'
              # This line executes PHPUnit mutations.
              - 'php vendor/bin/infection'
        - step:
            name: 'PHP Unit Tests 7.3'
            image:
              name: 'docker.trolldev.infra.polarnordic.com/visma/bitbucket-pipelines-m2-mod:7.3'
              username: $DOCKER_REGISTRY_USER
              password: $DOCKER_REGISTRY_PASSWORD
            script:
              - 'apk add -u tar'
              - 'tar -xf .package/build-php-7.3.tar'
              - 'rm -rf .package'
              - 'mkdir -p test-results'
              # This line executes PHPUnit and reports the results in the JUnit format for Bitbucket.
              - 'php vendor/bin/phpunit --log-junit ./test-results/phpunit-php-7.3.xml'
        - step:
            name: 'Prettier Code Style Checks'
            script:
              - 'apk add -u tar npm'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'node_modules/.bin/prettier --no-error-on-unmatched-pattern -c src'
        - step:
            name: 'Accessibility Test for HTML'
            script:
              - 'apk add -u tar npm'
              - 'tar -xf .package/build-php-7.4.tar'
              - '/usr/bin/env node accessibility.js'
        - step:
            name: 'ESLint Code Style Checks'
            script:
              - 'apk add -u tar npm'
              - 'tar -xf .package/build-php-7.4.tar'
              - 'node_modules/.bin/eslint --no-error-on-unmatched-pattern src'
definitions:
  caches:
    tools: .tmp/.tools
